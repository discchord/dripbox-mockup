import shutil
import socket
import asyncio
import time
import traceback
import json
import os
import datetime
import string
import random
from concurrent.futures import TimeoutError as ConnectionTimeoutError
import base64
import rsa
from cryptography.fernet import Fernet
import websockets
from websockets.exceptions import ConnectionClosedOK

from werkzeug.security import check_password_hash, generate_password_hash

import Dripbox
from Dripbox.Database import reload_shares
from Dripbox.FileManagement import get_hash

HELLO_MY_NAME_IS = socket.gethostname()
# print(HELLO_MY_NAME_IS)

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.connect(('8.8.8.8', 53))
    MY_IP = s.getsockname()[0]

# print(MY_IP)

CONNECTIONS = set()

GRABBED = False


class ConnectionHandler:
    websocket = None
    hostname = None
    uri = None
    state = 'Disconnect'
    f = None
    shares = []
    peers = []

    async def send(self, message):
        try:
            data = json.dumps(message, default=str)
            if self.f is not None:
                data = self.f.encrypt(data.encode())
            await self.websocket.send(data)
        except ConnectionClosedOK:
            print('Uncaught Disconnection has been Caught!')
            asyncio.get_event_loop().create_task(unregister(self))
        except:
            traceback.print_exc()

    async def recv(self):
        try:
            message = await self.websocket.recv()
            if self.f is not None:
                message = self.f.decrypt(message)
            data = json.loads(message)
            return data
        except:
            traceback.print_exc()

    async def file_send(self, filepath):
        sending = True
        self.state = 'TRANSFER'
        with open(filepath, 'rb') as f:
            while sending:
                buffer = f.read(8192)
                if not buffer:
                    buffer = ':EOF'
                    sending = False
                print('Sending Chunk')
                await self.websocket.send(buffer)
                await asyncio.sleep(0)

        self.state = 'Connected'

    async def file_recv(self, _id, filename, cache_time, cache_modified, cache_hash, cache_size):
        share, index = None, None
        for idx, wanted in enumerate(Dripbox.SHARES):
            if wanted['_id'] == _id:
                share = wanted
                index = idx
                break

        if share:
            self.state = 'TRANSFER'
            # Set up directories for our file to save to
            cache_path = os.path.normpath(os.path.join(os.getcwd(), f"cache/{filename}_{cache_modified}"))
            os.makedirs(cache_path)

            # Receive the file
            data_downloaded = 0
            with open(os.path.join(cache_path, filename), 'wb') as f:
                while True:
                    buffer = await self.websocket.recv()
                    if buffer == ':EOF':
                        break
                    f.write(buffer)
                    data_downloaded += 8192
                    progress = int(data_downloaded / cache_size * 100)
                    Dripbox.SHARES[index]['progress'] = progress
                    await Dripbox.GUI.refresh_ui()
                    await asyncio.sleep(0)  # Oops!

            if await get_hash(os.path.join(cache_path, filename)) != cache_hash:
                print(f"Download of {filename} from {self.hostname} was corrupt!")
                shutil.rmtree(cache_path)
                return

            mod_date = Dripbox.FileManagement.copy_to_dest(filename, share['share_path'], cache_path, cache_modified)

            cache_time = datetime.datetime.strptime(cache_time, '%Y-%m-%d %H:%M:%S')

            cache = {'cache_path': cache_path,
                     'cache_time': cache_time,
                     'cache_modified': mod_date.timestamp(),
                     'cache_hash': cache_hash,
                     'cache_size': cache_size}

            cache_list = share['cache']

            if len(cache_list) > 1:
                old_cache = cache_list.pop()
                shutil.rmtree(old_cache['cache_path'])

            cache_list.append(cache)

            Dripbox.db.Shares.update_one({'_id': _id}, {'$set': {'cache': cache_list, 'progress': 100}})
            Dripbox.GUI.status_message(f"Download complete for: {filename}")
            reload_shares()
            self.state = 'Connected'

    async def challenge_encode(self):
        ip_list = [int(i) for i in MY_IP.split('.')]
        ip_sum = sum(ip_list)
        ip_sum2 = ip_sum + int(self.websocket.remote_address[0].split('.')[-1])
        characters = string.ascii_letters + string.digits
        challenge = ''.join([random.choice(characters) for _ in range(ip_sum2 * 2)])

        mod = 3 - (len(challenge) % 3)
        if mod != 3:
            padding = '='*mod
            challenge += padding

        salt = challenge[ip_sum:ip_sum2][::-1]

        timestamp = datetime.datetime.utcnow()

        pass_hash = generate_password_hash(f"{salt}{Dripbox.SETTINGS['password']}{timestamp.timestamp()}")

        return timestamp, challenge, pass_hash

    async def challenge_decode(self, timestamp, challenge, pass_hash) -> bool:
        ip_list = [int(i) for i in self.websocket.remote_address[0].split('.')]
        ip_sum = sum(ip_list)
        ip_sum2 = ip_sum + int(MY_IP.split('.')[-1])
        salt = challenge[ip_sum:ip_sum2][::-1]
        #                                               2020-07-01 23:16:21.280111
        timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')
        when = datetime.datetime.utcnow() - datetime.timedelta(seconds=30)
        then = datetime.datetime.utcnow() + datetime.timedelta(seconds=30)
        if when < timestamp < then:
            return check_password_hash(pass_hash, f"{salt}{Dripbox.SETTINGS['password']}{timestamp.timestamp()}")
        else:
            print(f"Suspicious timestamp from {self.hostname}")
            return False

    async def login(self):
        try:
            self.websocket = await asyncio.wait_for(websockets.connect(self.uri), timeout=3)
        except ConnectionTimeoutError:
            # print(f"Connection timed out to {self.uri}")
            return
        except ConnectionRefusedError:
            # print(f"Connection refused to {self.uri}")
            return
        except:
            return

        await self.send({'hostname': HELLO_MY_NAME_IS})

        challenge = await self.recv()

        if 'challenge' not in challenge or 'hostname' not in challenge:
            # TODO: Fail2Ban
            return
        if len(challenge['challenge']) > 2048 or len(challenge['hostname']) > 1024:
            # TODO: Fail2Ban
            return

        self.hostname = challenge['hostname']

        if not await self.challenge_decode(challenge['date'], challenge['challenge'], challenge['api_key']):
            # TODO: Fail2Ban
            print(f"Invalid or Malicious Challenge from {self.hostname}")
            return

        encrypted_password = rsa.encrypt(Dripbox.SETTINGS['password'].encode(), rsa.PublicKey.load_pkcs1(challenge['public_key'].encode()))

        public_key, private_key = rsa.newkeys(512)

        password = {'password': base64.b64encode(encrypted_password).decode(), 'public_key': public_key.save_pkcs1().decode()}

        await self.send(password)

        confirmation = await self.recv()

        confirmed = confirmation.get('connection')
        if confirmed == 'authorized':
            aes_key = rsa.decrypt(base64.b64decode(confirmation['key'].encode()), private_key)
            self.f = Fernet(aes_key)
            self.state = 'Connected'
            Dripbox.GUI.status_message(f"Established a new connection to {self.hostname}!")
            asyncio.get_event_loop().create_task(status_update(self))
        else:
            print(f"Password Mismatch on {self.hostname}")

    async def welcome(self) -> bool:
        greeting = await self.recv()
        if 'hostname' not in greeting:
            # TODO: Fail2Ban
            return False
        if len(greeting['hostname']) > 1024:
            # TODO: Fail2Ban
            return False

        self.hostname = greeting['hostname']

        self.uri = f"ws://{self.websocket.remote_address[0]}:1111"

        timestamp, challenge, pass_hash = await self.challenge_encode()

        public_key, private_key = rsa.newkeys(512)

        challenge = {'hostname': HELLO_MY_NAME_IS,
                     'challenge': challenge,
                     'api_key': pass_hash,
                     'date': timestamp,
                     'public_key': public_key.save_pkcs1().decode()}

        await self.send(challenge)

        password = await self.recv()

        if 'password' not in password:
            # TODO: Fail2Ban
            return False
        if len(password['password']) > 1024:
            # TODO: Fail2Ban
            return False

        decrypted_password = rsa.decrypt(base64.b64decode(password['password'].encode()), private_key)

        if decrypted_password.decode() == Dripbox.SETTINGS['password']:
            aes_key = Fernet.generate_key()
            print(aes_key)
            await self.send({'connection': 'authorized',
                             'key': base64.b64encode(
                                 rsa.encrypt(aes_key, rsa.PublicKey.load_pkcs1(password['public_key'].encode()))
                             ).decode()})
            self.f = Fernet(aes_key)
            self.state = 'Connected'
            Dripbox.GUI.status_message(f"New connection from {self.hostname}!")
            asyncio.get_event_loop().create_task(self.listener())
            # TODO 8.3: Send status_update immediately to the new connection!
            asyncio.get_event_loop().create_task(status_update(self))
            return True
        else:
            # TODO: Fail2Ban
            await self.send({'connection': 'unauthorized'})
            return False

    async def listener(self):
        try:
            async for message in self.websocket:
                message = self.f.decrypt(message)
                data = json.loads(message)
                op_type = data.get('op_type')

                if op_type == 'status':
                    print(f"{self.hostname} Status: \n {data['connections']} \n {data['shares']}")
                    self.peers = data['connections']

                    # NEVER OVER ENGINEER -- ENGINEERING HARDER JUST BREAKS THINGS!
                    # for peer in self.peers:
                    #     await connect(peer['uri'])

                    # Check the shares to see if we are looking for any changes to those files
                    if self.shares != data['shares']:
                        print(f"Shares Changed on {self.hostname}")
                        self.shares = data['shares']

                        if HELLO_MY_NAME_IS != 'Client':
                            Dripbox.GUI.rebuild_ui()

                        for share in self.shares:
                            wanted = Dripbox.db.Shares.find_one({'_id': share['_id']})
                            if wanted:
                                try:
                                    if wanted['cache'][-1]['cache_hash'] != share['cache_hash']:
                                        print(f"{share['filename']} cache is different on {self.hostname}")
                                        if wanted['cache'][-1]['cache_time'] < datetime.datetime.strptime(share['cache_time'], '%Y-%m-%d %H:%M:%S'):
                                            print(f"Our copy is older! Requesting update from {self.hostname}")
                                            await self.send({'op_type': 'request', '_id': share['_id'], 'filename': share['filename']})
                                except IndexError:
                                    if len(wanted['cache']) == 0:
                                        await self.send({'op_type': 'request', '_id': share['_id'], 'filename': share['filename']})

                if op_type == 'request':
                    Dripbox.GUI.status_message(f"{self.hostname} Requests File: {data['filename']}")
                    # Prep all the file details that the recipient needs
                    for share in Dripbox.SHARES:
                        if share['_id'] == data['_id']:
                            filename = share['filename']
                            cache_time = share['cache'][-1]['cache_time']
                            cache_modified = share['cache'][-1]['cache_modified']
                            cache_hash = share['cache'][-1]['cache_hash']
                            cache_path = share['cache'][-1]['cache_path']
                            cache_size = share['cache'][-1]['cache_size']

                            await self.send({'op_type': 'sending',
                                             '_id': share['_id'],
                                             'filename': filename,
                                             'cache_time': cache_time,
                                             'cache_modified': cache_modified,
                                             'cache_hash': cache_hash,
                                             'cache_size': cache_size})

                            # Send the file
                            await self.file_send(os.path.join(cache_path, filename))

                if op_type == 'sending':
                    Dripbox.GUI.status_message(f"{self.hostname} confirms sending file: {data['filename']}")
                    # Take all the details for the file, and receive it!
                    await self.file_recv(data['_id'], data['filename'], data['cache_time'],
                                         data['cache_modified'], data['cache_hash'], data['cache_size'])

        except websockets.exceptions.ConnectionClosed:
            print(f"Connection closed from {self.hostname}")
            await unregister(self)
        except:
            traceback.print_exc()
            await unregister(self)

    async def close(self):
        self.state = 'Disconnected'
        try:
            await self.websocket.close()
        except:
            traceback.print_exc()


class ServerHandler(ConnectionHandler):
    def __init__(self, websocket):
        self.websocket = websocket


class ClientHandler(ConnectionHandler):
    def __init__(self, uri):
        self.uri = uri


async def connect(uri, override=False):
    if uri == f"ws://{MY_IP}:1111" and override is False:
        return

    for connection in CONNECTIONS:
        if uri == connection.uri:
            return

    connection = ClientHandler(uri)
    await connection.login()
    if connection.state == 'Connected':
        CONNECTIONS.add(connection)
        asyncio.get_event_loop().create_task(connection.listener())


async def port_scan():
    if not MY_IP[:3] == '192' and not MY_IP[:3] == '10.' and not MY_IP[:3] == '172':
        print('This is not a private network! SHUTTING DOWN!')
        exit()

    ip_range = MY_IP.split('.')
    ip_range.pop()
    ip_range = '.'.join(ip_range)

    i = 1
    task_list = []
    while i < 256:
        target_ip = f"{ip_range}.{i}"
        uri = f"ws://{target_ip}:1111"
        task_list.append(connect(uri))
        if not i % 5:
            await asyncio.gather(*task_list)
            task_list = []

        i += 1
        await asyncio.sleep(0)


async def register_client(websocket, _):
    connection = ServerHandler(websocket)
    done = False
    while True:
        if not done:
            if await connection.welcome():
                CONNECTIONS.add(connection)
                done = True

        await asyncio.sleep(0)


async def unregister(connection):
    await connection.close()
    try:
        CONNECTIONS.remove(connection)
    except:
        traceback.print_exc()


async def status_update(new_connection=None):
    while True:
        if Dripbox.STATE == 'connected':

            connection_list = [{'hostname': connection.hostname, 'uri': connection.uri} for connection in CONNECTIONS]

            share_list = []
            for share in Dripbox.SHARES:
                try:
                    share_list.append({'_id': share['_id'],
                                       'filename': share['filename'],
                                       'cache_time': share['cache'][-1]['cache_time'],
                                       'cache_modified': share['cache'][-1]['cache_modified'],
                                       'cache_hash': share['cache'][-1]['cache_hash'],
                                       'cache_size': share['cache'][-1]['cache_size']})
                except IndexError:
                    pass
            if new_connection is not None:
                if new_connection.state == 'Connected':
                    await new_connection.send({'op_type': 'status',
                                               'hostname': HELLO_MY_NAME_IS,
                                               'connections': connection_list,
                                               'shares': share_list})
                break

            if CONNECTIONS:
                Dripbox.GUI.status_message(f"Online - Connected to {len(CONNECTIONS)} Peers")
            else:
                Dripbox.GUI.status_message('Online - Not Connected to any Peers')

            try:
                for connection in CONNECTIONS:
                    if connection.state == 'Connected':
                        Dripbox.GUI.status_message(f"{connection.hostname} state is: {connection.state}")
                        await connection.send({'op_type': 'status',
                                               'hostname': HELLO_MY_NAME_IS,
                                               'connections': connection_list,
                                               'shares': share_list})
            except RuntimeError:
                pass

        await asyncio.sleep(60)


def list_all_snags():
    # stage1 = [connection.shares for connection in CONNECTIONS]
    # print(f"stage1: {stage1}")
    # # [int(x) for line in data for x in line.split()]
    # stage2 = [share for share_list in stage1 for share in share_list]
    # print(f"stage2: {stage2}")
    # stage3 = {tuple(share.items()) for share in stage2}
    # print(f"stage3: {stage3}")
    # stage4 = [dict(t) for t in stage3]
    # print(f"stage4: {stage4}")
    # print('___________________________')
    # christ_is_this_real_life = [share for share_list in [connection.shares for connection in CONNECTIONS] for share in share_list]
    # print('listing all shares...')
    all_snags = [dict(t) for t in {tuple(share.items()) for share in [share for share_list in [connection.shares for connection in CONNECTIONS] for share in share_list]}]

    for snag in all_snags:
        for share in Dripbox.SHARES:
            if snag['_id'] == share['_id']:
                all_snags.remove(snag)

    return all_snags

start_server = websockets.serve(register_client, MY_IP, 1111)

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().create_task(status_update())
    asyncio.get_event_loop().run_forever()
