import PySimpleGUI as sg
import asyncio
import os

from tinymongo import TinyMongoClient
from tinymongo.serializers import DateTimeSerializer
from tinydb_serialization import SerializationMiddleware

from Dripbox.GUI import ui, create_layout, refresh_ui, status_message
from Dripbox.ImageSet import icon
from Dripbox.Background import background
from Dripbox.FileManagement import check_local_files
from Dripbox.Networking import port_scan, status_update, start_server
from Dripbox.ScratchPad import progress_test


class TimMongoClient(TinyMongoClient):
    @property
    def _storage(self):
        serializer = SerializationMiddleware()
        serializer.register_serializer(DateTimeSerializer(), 'TinyDate')
        return serializer


__client__ = TimMongoClient(os.path.join(os.getcwd(), 'dripbox.db'))
db = __client__['dripbox']

SETTINGS = db.Settings.find_one({'_id': 'settings'})
if not SETTINGS:
    SETTINGS = {'_id': 'settings', 'password': ''}
    db.Settings.insert(SETTINGS)
SHARES = [i for i in db.Shares.find()]

if SETTINGS['password']:
    STATE = 'connected'
else:
    STATE = 'init'
    status_message('Offline')

sg.change_look_and_feel('mine')
window = sg.Window('Dripbox', icon=icon, finalize=True, border_depth=0).Layout(create_layout())


async def looper():
    # await asyncio.gather(
    #     *[background(), ui(), check_local_files(), port_scan(), status_update(), start_server, refresh_ui()]
    # )

    # DEBUG
    await asyncio.gather(
        *[background(), ui(), check_local_files(), port_scan(), status_update(), refresh_ui()]
    )


def main():
    asyncio.get_event_loop().run_until_complete(looper())


if __name__ == '__main__':
    main()
