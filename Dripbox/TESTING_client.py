import datetime
import asyncio
import os

import Dripbox.Networking


async def client_things():
    client_loop = [Dripbox.Networking.connect(f"ws://{Dripbox.Networking.MY_IP}:1111", override=True),
                   Dripbox.Networking.status_update(), reconnect()]
    await asyncio.gather(*client_loop)


async def reconnect():
    await asyncio.sleep(10)
    while True:
        if len(Dripbox.Networking.CONNECTIONS) == 0:
            asyncio.get_event_loop().create_task(Dripbox.Networking.connect(f"ws://{Dripbox.Networking.MY_IP}:1111", override=True))
            await asyncio.sleep(4)
        await asyncio.sleep(1)


def tester_main():
    Dripbox.Networking.HELLO_MY_NAME_IS = 'Client'
    Dripbox.SHARES = [{"filename": "__init__.py", "share_path": "C:\\Users\\Tim\\PycharmProjects\\ACP - Intermediate\\Dripbox\\__init__.py", "progress": 100, "cache": [{"cache_time": "{TinyDate}:2020-10-07T23:13:33", "cache_modified": 1602112412.9285505, "cache_hash": "c5df6212a833c17f916dbbfbbbbd2b2f", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\ACP - Intermediate\\cache\\__init__.py_1602130413.793664", "cache_size": 1792}, {"cache_time": "{TinyDate}:2020-10-08T20:21:49", "cache_modified": 1602188207.370551, "cache_hash": "a76f853aed499fb7941dac8ff3fec7dc", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\ACP - Intermediate\\cache\\__init__.py_1602206509.865458", "cache_size": 1805}], "_id": "56adcc3e037311ebbf3e0c9d9210af05"},
                      {"filename": "Dripbox.iml", "share_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\.idea\\Dripbox.iml", "progress": 100, "cache": [{"cache_time": datetime.datetime.utcnow().replace(microsecond=0), "cache_modified": 1574972832.6603065, "cache_hash": "539261add9453a44df076b305c70f312", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\dripbox\\cache\\Dripbox.iml_1600230646.180173", "cache_size": 521}], "_id": "7aea8caef7ab11ea97cc001a7dda7113"},
                      {"filename": "TESTING_client.py", "share_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\Dripbox\\TESTING_client.py", "progress": 100, "cache": [{"cache_time": datetime.datetime.utcnow().replace(microsecond=0), "cache_modified": 1600238176.818046, "cache_hash": "824be94ce01bd2583662f96dc71ed2a9", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\dripbox\\cache\\TESTING_client.py_1600256177.69728", "cache_size": 9148394832984}, {"cache_time": datetime.datetime.utcnow().replace(microsecond=0), "cache_modified": 1600289947.3816302, "cache_hash": "6e574546bd835abaf6ff0db840852f44", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\dripbox\\cache\\TESTING_client.py_1600307948.251589", "cache_size": 91480000000}], "_id": "ee0e44f4f6dc11eaa76d001a7dda7113"},
                      {"filename": "GRAB.ME", "share_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\GRAB.ME", "progress": 100, "cache": [{"cache_time": datetime.datetime.utcnow().replace(microsecond=0), "cache_modified": 1595373019.5800087, "cache_hash": "e058ace74be35f963a92a9806a8afdd8", "cache_path": "C:\\Users\\Tim\\PycharmProjects\\Dripbox\\dripbox\\cache\\GRAB.ME_1600141972.75707", "cache_size": 20480000}], "_id": "05a2a5c6f6dd11ea9378001a7dda7113"}]
    asyncio.get_event_loop().run_until_complete(client_things())


if __name__ == '__main__':
    tester_main()

