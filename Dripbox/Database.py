import asyncio
import os
import shutil
import datetime
import time
import traceback

import Dripbox
import Dripbox.GUI
import Dripbox.FileManagement


def req_share(identity: tuple, share_path):
    _id, filename = identity
    share_path = os.path.normpath(share_path)
    if not os.path.exists(share_path):
        Dripbox.GUI.alert_snag('Directory Not Found!')
        return False
    if Dripbox.db.Shares.find_one({'_id': _id}):
        Dripbox.GUI.alert_snag('This is a duplicate of an already shared file!')
        return False
    if Dripbox.db.Shares.find_one({'share_path': share_path, 'filename': filename}):
        Dripbox.GUI.alert_snag('This path is already used by another shared file!')
        return False

    try:
        with open(os.path.join(share_path, 'touch'), 'a+') as f:
            f.write('Testing Write!')
        os.remove(os.path.join(share_path, 'touch'))
    except PermissionError:
        Dripbox.GUI.alert_snag('Insufficient Permissions to Save in this Directory!')
        return False
    except Exception as e:
        Dripbox.GUI.alert_snag(f"Error while testing this directory for write permission! {e}")
        return False

    Dripbox.db.Shares.insert({'_id': _id, 'filename': filename, 'share_path': share_path, 'cache': [], 'progress': 0})
    reload_shares(True)
#   Clear out the copies of shares in all instances, so that
#   all status updates from peers will be treated as new.
#     for connection in Dripbox.Networking.CONNECTIONS:
#         connection.shares = []



async def add_share(share_path):
    share_path = os.path.normpath(share_path)
    if not os.path.isfile(share_path):
        Dripbox.GUI.alert_share('File Not Found')
        return False

    if Dripbox.db.Shares.find_one({'share_path': share_path}):
        Dripbox.GUI.alert_share('Duplicate Share')
        return False
    filename, cache_time, cache_modified, cache_hash, cache_path, cache_size = await Dripbox.FileManagement.create_cache(share_path)

    if not os.path.isfile(os.path.join(cache_path, filename)):
        Dripbox.GUI.alert_share('Failed to Create Cache')
        return False

    Dripbox.db.Shares.insert({'filename': filename,
                              'share_path': share_path,
                              'progress': 100,
                              'cache': [{'cache_time': cache_time,
                                         'cache_modified': cache_modified,
                                         'cache_hash': cache_hash,
                                         'cache_path': cache_path,
                                         'cache_size': cache_size}]
                              })
    reload_shares(True)
    return True


def remove_share(_id):
    try:
        share = Dripbox.db.Shares.find_one({'_id': _id})
        if not share:
            Dripbox.GUI.status_message(f"Could not find share with ID {_id}")

        cache_list = share['cache']
        for cache in cache_list:
            Dripbox.FileManagement.remove_cache(cache)

        Dripbox.db.Shares.delete_one({'_id': _id})
        reload_shares(True)
        return True
    except Exception as e:
        traceback.print_exc()
        Dripbox.GUI.status_message(f"Exception while trying to remove share: {e}")
        return False


def revert_share(_id):
    share = Dripbox.db.Shares.find_one({'_id': _id})
    if not share:
        Dripbox.GUI.status_message(f"Could not find share with ID {_id}")
        return False

    Dripbox.FileManagement.remove_cache(share['cache'][-1])
    new_old_cache = share['cache'][0]

    mod_date = Dripbox.FileManagement.copy_to_dest(filename=share['filename'],
                                                   share_path=share['share_path'],
                                                   cache_path=new_old_cache['cache_path'],
                                                   cache_modified=new_old_cache['cache_modified'])

    new_old_cache['cache_time'] = datetime.datetime.utcnow()
    new_old_cache['cache_modified'] = mod_date.timestamp()

    Dripbox.db.Shares.update_one({'_id': _id}, {'$set': {'cache': [new_old_cache]}})
    reload_shares()
    return True


def reload_shares(rebuild=False):
    Dripbox.SHARES = [i for i in Dripbox.db.Shares.find()]

    if rebuild:
        Dripbox.GUI.rebuild_ui()

    asyncio.get_event_loop().create_task(Dripbox.GUI.refresh_ui())


def save_settings():
    Dripbox.db.Settings.update({'_id': 'settings'}, {'$set': {'password': Dripbox.SETTINGS['password']}})
