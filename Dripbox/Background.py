import random
import asyncio

import Dripbox


async def background():
    while True:
        rando = random.randint(2, 20000)
        if Dripbox.STATE == 'login':
            Dripbox.window['login_status'].update(rando)
        else:
            try:
                await Dripbox.GUI.check_window()
                Dripbox.window['connection_status'].update(rando)
            except:
                pass
        await asyncio.sleep(0)
