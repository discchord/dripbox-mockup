import os
import shutil
import datetime
import hashlib
import asyncio
import time
import traceback

import Dripbox
import Dripbox.Database


async def get_hash(file):
    with open(file, 'rb') as f:
        md5 = hashlib.md5()
        while True:
            buffer = f.read(8192)
            if not buffer:
                break
            md5.update(buffer)
            await asyncio.sleep(0)
        return md5.hexdigest()


async def create_cache(share_path):
    filename = os.path.basename(share_path)
    cache_time = datetime.datetime.utcnow()
    cache_modified = os.path.getmtime(share_path)
    cache_hash = await get_hash(share_path)
    cache_path = os.path.normpath(os.path.join(os.getcwd(), f"cache/{filename}_{cache_time.timestamp()}"))
    os.makedirs(cache_path)
    shutil.copy2(share_path, os.path.join(cache_path, filename))
    cache_size = os.path.getsize(os.path.join(cache_path, filename))

    return filename, cache_time, cache_modified, cache_hash, cache_path, cache_size


def copy_to_dest(filename: str, share_path: str, cache_path: str, cache_modified: float) -> datetime.datetime.timestamp:
    shutil.copy2(os.path.join(cache_path, filename), share_path)
    mod_date = datetime.datetime.fromtimestamp(float(round(cache_modified)))
    mod_time = time.mktime(mod_date.timetuple())
    os.utime(share_path, (mod_time, mod_time))
    return mod_date


def remove_cache(old_cache):
    try:
        shutil.rmtree(old_cache['cache_path'])
    except FileNotFoundError:
        pass
    except:
        traceback.print_exc()


async def add_cache(share):
    filename, cache_time, cache_modified, cache_hash, cache_path, cache_size = await create_cache(share['share_path'])

    new_cache = {'cache_time': cache_time,
                 'cache_modified': cache_modified,
                 'cache_hash': cache_hash,
                 'cache_path': cache_path,
                 'cache_size': cache_size}

    cache_list = share['cache']
    cache_list.append(new_cache)

    if len(cache_list) > 2:
        old_cache = cache_list.pop(0)
        remove_cache(old_cache)

    Dripbox.db.Shares.update({'_id': share['_id']}, {'$set': {'cache': cache_list}})
    Dripbox.Database.reload_shares()


async def check_local_files():
    while True:
        for share in Dripbox.SHARES:
            try:
                print(f"Checking {share['filename']}...")
                if share['cache'][-1]['cache_modified'] != os.path.getmtime(share['share_path']):
                    print(f"Modified Date has changed for {share['filename']}")
                    if share['cache'][-1]['cache_hash'] != await get_hash(share['share_path']):
                        print(f"Hash has changed for {share['filename']}")
                        await add_cache(share)
                    else:
                        share['cache'][-1]['cache_modified'] = os.path.getmtime(share['share_path'])
                        Dripbox.db.Shares.update_one({'_id': share['_id']}, {'$set': {'cache': share['cache']}})

                    Dripbox.Database.reload_shares()

            except IndexError:
                pass
            except Exception as e:
                print(repr(e))
        await asyncio.sleep(10*60)
