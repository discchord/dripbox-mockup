import sys
import asyncio
import traceback

import PySimpleGUI as sg

import Dripbox
import Dripbox.Database
import Dripbox.ImageSet

__MESSAGE__ = ''

MY_PINK = '#C416C1'
MY_GREEN = '#75C42C'
MY_BLUE = '#2CBfC4'
MY_ORANGE = '#C7562C'
OFF_GREY = '#FAFAFA'
OFF_BLACK = '#222222'

ACCENT = MY_GREEN
HIGHLIGHT = MY_PINK
WARN = MY_ORANGE
TEXTCOLOR = OFF_GREY
BGCOLOR = OFF_BLACK


# https://pysimplegui.readthedocs.io/en/latest/cookbook/#making-changes-to-themes-adding-your-own-themes <--- This says Copy-Paste!
my_own_theme = {'BACKGROUND': BGCOLOR,
                'TEXT': TEXTCOLOR,
                'INPUT': HIGHLIGHT,
                'SCROLL': ACCENT,
                'TEXT_INPUT': BGCOLOR,
                'BUTTON': (TEXTCOLOR, ACCENT),
                'PROGRESS': (HIGHLIGHT, BGCOLOR),  # This is definitely not right...
                'BORDER': 1,
                'SLIDER_DEPTH': 0,
                'PROGRESS_DEPTH': 0}

Dripbox.sg.theme_add_new('mine', my_own_theme)

FONT = ('Montserrat', 12)
FONT_BOLD = ('Montserrat', 12, 'bold')
FONT_BIGGER = ('Montserrat', 14)
FONT_BIGGEST = ('Montserrat', 16, 'bold')
BUTTON_PROPS = {'size': (7, 2), 'font': FONT_BOLD, 'border_width': 0}
BUTTON_BIG = {**BUTTON_PROPS, 'size': (10, 2)}


def create_layout():
    layout_login = [
        [sg.Image(data=Dripbox.ImageSet.banner)],
        [sg.Text('')],
        [sg.Text('Password:', font=FONT_BIGGEST, pad=(15, 10)), sg.InputText(password_char='*', size=(30, 1), font=FONT_BIGGEST, key='password')],
        [sg.Checkbox('Save Password', key='save_pass', font=FONT_BIGGEST, pad=(15, 10))],
        [sg.Text('', key='login_status', size=(20, 1), font=FONT_BOLD, text_color=WARN, pad=(15, 10))],
        [sg.Column([[sg.Button('Connect', **BUTTON_BIG, pad=(20, 5)), sg.Button('Quit', **BUTTON_BIG)]], justification='c')]
    ]

    layout_main = [
        [sg.Image(data=Dripbox.ImageSet.inner_banner, pad=(10, 6)),
         sg.Column(
             [[
                 sg.Button('Snag', **BUTTON_PROPS, pad=(5, 0)),
                 sg.Button('Share', **BUTTON_PROPS)
             ]])
        ]
    ]

    share_list = []

    for index, share in enumerate(Dripbox.SHARES):
        share_list.append(
            [sg.Button('', image_data=Dripbox.ImageSet.delete, button_color=('white', BGCOLOR), pad=(8, 10),
                       border_width=0, key=('remove', share['_id'])),
             sg.Button('', image_data=Dripbox.ImageSet.revert, button_color=('white', BGCOLOR), pad=(8, 10),
                       border_width=0, key=('revert', share['_id'])),
             sg.Text(share['filename'], font=FONT, size=(25, 1), pad=(10, 10)),
             sg.ProgressBar(100, orientation='h', size=(37, 20), pad=(0, 10), key=('progress', share['_id']), border_width=0)
             ])

    col_share_list = sg.Column(share_list)

    if len(Dripbox.SHARES) > 7:
        share_scroll = {'size': (875, 310), 'scrollable': True, 'vertical_scroll_only': True}
    else:
        share_scroll = {'size': (892, 310), 'scrollable': False}

    pane_shares = [sg.Column([[col_share_list]], pad=(10, 5), **share_scroll)]

    layout_main.append(pane_shares)

    status_bar = sg.Frame(layout=[[
        sg.Text(deque_message(), key='connection_status', size=(50, 1), pad=(10, 0), background_color=TEXTCOLOR, text_color=BGCOLOR)]],
        background_color=TEXTCOLOR, pad=(0, 0), relief=sg.RELIEF_SUNKEN, title='', border_width=1, key='pane_status')

    layout_share = [
        [sg.Image(data=Dripbox.ImageSet.inner_banner, pad=(10, 6))],
        [sg.Text('')],
        [sg.Text('Select the file you would like to share:', font=FONT_BIGGER, pad=(10, 5), size=(75, 1))],
        [sg.Input('', key='new_share', font=FONT_BIGGEST, size=(57, 1), pad=(10, 5)),
         sg.FileBrowse(target='new_share', font=FONT_BOLD, size=(7, 2), pad=(8, 0))],
        [sg.Text('', key='share_status', font=FONT_BOLD, text_color=WARN, size=(40, 1), pad=(10, 5))],
        [sg.Button('Confirm', **BUTTON_PROPS, pad=(10, 5)), sg.Button('Cancel', **BUTTON_PROPS)],
        [sg.Text('', pad=(0, 42))]

    ]

    layout_snag = [
        [sg.Image(data=Dripbox.ImageSet.inner_banner, pad=(10, 6))],
        [sg.Text('')],
        [sg.Text('Select the directory you would like to save this file to:', font=FONT_BIGGER, pad=(10, 5), size=(75, 1))],
        [sg.Input('', key='new_snag', font=FONT_BIGGEST, size=(57, 1), pad=(10, 5)),
         sg.FolderBrowse(target='new_snag', font=FONT_BOLD, size=(7, 2), pad=(8, 0))],
        [sg.Text('', key='snag_status', font=FONT_BOLD, text_color=WARN, size=(40, 1), pad=(10, 5))],
        [sg.Button('Confirm', **BUTTON_PROPS, pad=(10, 5), key='confirm_snag'),
         sg.Button('Cancel', **BUTTON_PROPS, key='cancel_snag')],
        [sg.Text('', pad=(0, 42))]

    ]

    snag_list = []
    all_the_snags = Dripbox.Networking.list_all_snags()
    for snag in all_the_snags:
        snag_list.append(
            [sg.Button('', image_data=Dripbox.ImageSet.download, button_color=('white', BGCOLOR), pad=(8, 10),
                       border_width=0, key=('snag', snag['_id'], snag['filename'])),
             sg.Text(snag['filename'], font=FONT, size=(70, 1), pad=(10, 10)),
             sg.Text(humanize_size(snag['cache_size']), size=(12, 1), font=FONT, justification='r')
             ])

    col_snag_list = sg.Column(snag_list)

    if len(all_the_snags) > 7:
        snag_scroll = {'size': (875, 310), 'scrollable': True, 'vertical_scroll_only': True}
    else:
        snag_scroll = {'size': (892, 310), 'scrollable': False}

    pane_snags = [sg.Column([[col_snag_list]], pad=(10, 5), **snag_scroll)]

    layout_snags = [
        [sg.Image(data=Dripbox.ImageSet.inner_banner, pad=(10, 6)),
         sg.Column(
            [[
                sg.Text('', **BUTTON_PROPS, pad=(6, 0)),
                sg.Button('Cancel', **BUTTON_PROPS, key='cancel_snags')
            ]])
         ],
        pane_snags
    ]

    if Dripbox.STATE == 'init':
        visibility = [True, False]
    else:
        visibility = [False, True]

    col_login = sg.Column(layout_login, key='panel_login', visible=visibility[0])
    col_main = sg.Column(layout_main, key='panel_main', visible=visibility[1])
    col_share = sg.Column(layout_share, key='panel_share', visible=False)
    col_snags = sg.Column(layout_snags, key='panel_snags', visible=False)
    col_snag = sg.Column(layout_snag, key='panel_snag', visible=False)

    layout = [
        [sg.Pane([col_login, col_main, col_share, col_snags, col_snag], relief=sg.RELIEF_FLAT)],
        [status_bar]
    ]

    return layout


def rebuild_ui():
    try:
        new_window = sg.Window('Dripbox', border_depth=0, icon=Dripbox.ImageSet.icon,
                               finalize=True, location=Dripbox.window.current_location()).Layout(create_layout())
        Dripbox.window.close()
        Dripbox.window = new_window
        asyncio.get_event_loop().create_task(refresh_ui())
    except:
        traceback.print_exc()


async def refresh_ui():
    await check_window()
    Dripbox.window['pane_status'].expand(expand_x=True, expand_y=False)
    await check_window()
    for share in Dripbox.SHARES:
        if len(share['cache']) > 1:
            Dripbox.window[('revert', share['_id'])].update(disabled=False)
        else:
            Dripbox.window[('revert', share['_id'])].update(disabled=True)
        if share['progress'] == 100:
            Dripbox.window[('progress', share['_id'])].update(visible=False, current_count=0)
        else:
            Dripbox.window[('progress', share['_id'])].update(visible=True, current_count=share['progress'])


def alert_share(message):
    Dripbox.window['share_status'].update(message)


def alert_snag(message):
    Dripbox.window['snag_status'].update(message)


def status_message(message):
    Dripbox.GUI.__MESSAGE__ = message
    try:
        print('UPDATING STATUS')
        if Dripbox.window['connection_status'].Widget is not None:
            Dripbox.window['connection_status'].update(message)
    except AttributeError:
        pass
    except:
        traceback.print_exc()
    asyncio.get_event_loop().create_task(delayed_deque())


def deque_message():
    if Dripbox.GUI.__MESSAGE__:
        return Dripbox.GUI.__MESSAGE__
    else:
        return 'Online - Not Connected'


async def delayed_deque():
    await check_window()
    Dripbox.window['connection_status'].update(deque_message())


async def check_window():
    while Dripbox.window['panel_main'].Widget is None:
        await asyncio.sleep(0)
    return True


# def humanize_size(n, pow=0,b=1000,u='B'):
#     pre = [''] + [p + 'i' for p in 'KMGTPEZY']
#     r, f = min(int(log(max(n*b**pow, 1), b)), len(pre)-1),'{:,.%if} %s%s'
#     return (f % (abs(r % (-r-1)), pre[r], u)).format(n*b**pow/b**float(r))


def humanize_size(n):
    byte_size = n / 1024

    if byte_size > 1000000000:
        human_readable = round(byte_size/1000000000, 2)
        magnitude = 'TB'
    elif byte_size > 1000000:
        human_readable = round(byte_size/1000000, 2)
        magnitude = 'GB'
    elif byte_size > 1000:
        human_readable = round(byte_size/1000, 2)
        magnitude = 'MB'
    else:
        human_readable = round(byte_size, 2)
        magnitude = 'KB'

    return f"{human_readable} {magnitude}"


async def ui():
    do_want = None
    while True:
        event, value = Dripbox.window.read(timeout=10)
        if Dripbox.STATE == 'init':
            Dripbox.STATE = 'login'
        if event in (None, 'Quit'):
            sys.exit()
        elif event == 'Connect':
            if value['password']:
                if value['save_pass']:
                    Dripbox.SETTINGS['password'] = value['password']
                    Dripbox.Database.save_settings()
                Dripbox.STATE = 'connected'
                Dripbox.window['panel_login'].update(visible=False)
                Dripbox.window['panel_main'].update(visible=True)
        elif event == 'Share':
            Dripbox.window['panel_main'].update(visible=False)
            Dripbox.window['panel_share'].update(visible=True)
        elif event == 'Cancel':
            Dripbox.window['panel_main'].update(visible=True)
            Dripbox.window['panel_share'].update(visible=False)
            Dripbox.window['new_share'].update('')
        elif event == 'Confirm':
            if value['new_share']:
                if await Dripbox.Database.add_share(value['new_share']):
                    status_message('File was successfully cached for sharing!')
        elif 'remove' in event:
            response = sg.popup_ok_cancel('Are you sure you would like to stop sharing this file?\n\n'
                                          'Peers will no longer receive '
                                          'changes to this file.\n'
                                          'and all local cache files will be deleted.\n\n'
                                          'Destination file remain unaffected!', no_titlebar=True, background_color=TEXTCOLOR,
                                          text_color=BGCOLOR, font=FONT, button_color=('white', WARN), location=get_target_location())
            if response == 'OK':
                if Dripbox.Database.remove_share(event[1]):
                    status_message(f'Share Removed!')

        elif 'revert' in event:
            response = sg.popup_ok_cancel('Are you sure you would like to revert this file?\n\n'
                                          'Your destination file will be replaced with\n'
                                          'an older cached version of the file.\n\n'
                                          'This change will affect all Peers!', no_titlebar=True, background_color=TEXTCOLOR,
                                          text_color=BGCOLOR, font=FONT, button_color=('white', WARN), location=get_target_location())
            if response == 'OK':
                if Dripbox.Database.revert_share(event[1]):
                    status_message(f'Share Reverted!')
        elif event == 'Snag':
            Dripbox.window['panel_main'].update(visible=False)
            Dripbox.window['panel_snags'].update(visible=True)
        elif event == 'cancel_snags':
            Dripbox.window['panel_main'].update(visible=True)
            Dripbox.window['panel_snags'].update(visible=False)
        elif event == 'confirm_snag':
            if Dripbox.Database.req_share(do_want, value['new_snag']):
                status_message(f"New share {do_want[1]} requested from Peers!")
        elif event == 'cancel_snag':
            Dripbox.window['panel_snags'].update(visible=True)
            Dripbox.window['panel_snag'].update(visible=False)
            Dripbox.window['new_snag'].update('')
        elif 'snag' in event:
            do_want = (event[1], event[2])
            Dripbox.window['panel_snags'].update(visible=False)
            Dripbox.window['panel_snag'].update(visible=True)
        elif event != '__TIMEOUT__':
            print(f"{event} - {value}")
        await asyncio.sleep(0)


def get_target_location():
    location = Dripbox.window.current_location()
    size = Dripbox.window.size
    target_location_x = (location[0] + (size[0] / 2)) - 205
    target_location_y = (location[1] + (size[1] / 2)) - 55
    return target_location_x, target_location_y

